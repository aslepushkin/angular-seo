(function(){

    'use strict';

    angular
        .module('sampleApp',['ui.router'])
        .config(init)
        .constant('appStates',{
            home: {
                url: '/',
                templateUrl: 'javascripts/home/template.html',
                controller: 'HomeController',
                controllerAs: 'vm'
            },
            step: {
                url: '/step/{id}',
                templateUrl: 'javascripts/step/template.html',
                controller: 'StepController',
                controllerAs: 'vm',
                resolve: {
                    id: function ($stateParams) {
                        return $stateParams.id;
                    }
                }
            },
            error: {
                url: '/error',
                templateUrl: 'javascripts/error/template.html'
            }
        })
        .constant('pageDefaults',{
            title: 'Seven SEO Steps',
            description: 'Seven easy steps to optimize your angular application for the google crawlers. You may use it as sample.'
        });

    /* @ngInject */
    function init($locationProvider){
        //$locationProvider.html5Mode(true).hashPrefix('!');
        $locationProvider.html5Mode(true);
        //$locationProvider.html5Mode({
        //    enabled: true,
        //    requireBase: false
        //});
    }
})();
