(function(){

    'use strict';

    angular
        .module('sampleApp')
        .factory('StepService', StepService);

    /* @ngInject */
    function StepService($http){
        var service = {
            getList: getList,
            getStep: getStep
        };

        return service;

        //////////

        function getList(){
            return $http.get('/steps').then(function(response){
                return response.data;
            });
        }

        function getStep(id){
            return $http.get('/steps/'+id).then(function(response){
                return response.data;
            });
        }

    }

})();

