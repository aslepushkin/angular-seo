(function(){

    'use strict';

    angular
        .module('sampleApp')
        .controller('StepController', StepController);

    /* @ngInject */
    function StepController($state, id, StepService, PageMetaService, pageDefaults){
        var vm = this;

        StepService.getStep(id).then(function(step){
            vm.step = step;
            PageMetaService.setMeta({
                title: pageDefaults.title + ' | ' + step.title,
                description: step.description,
                ogTitle: pageDefaults.title + ' | ' + step.title,
                ogDescription: step.description
                //,
                //canonical: 'http://localhost:3000/step/' + step.id
            });
        }, function(){
            $state.go('error');
        });

    }

})();

