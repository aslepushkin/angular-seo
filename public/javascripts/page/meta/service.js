(function(){

    'use strict';

    angular
        .module('sampleApp')
        .factory('PageMetaService', PageMetaService);

    /* @ngInject */
    function PageMetaService(){
        var _meta = {
            title: '',
            description: '',
            ogTitle: '',
            ogDescription: ''
            //,
            //canonical: ''
        };
        var service = {
            setMeta: setMeta,
            clearMeta: clearMeta
        };

        activate();

        return service;

        //////////

        function activate(){
            for(var i in _meta){
                service[i] = props(i);
            }
        }

        function props(name){
            return function(str){
                if(str !== null && typeof str !== 'undefined'){
                    _meta[name] = str;
                }
                return _meta[name];
            }
        }

        function setMeta(meta){
            if (!meta) return;
            for(var i in _meta){
                _meta[i] = meta[i] || '';
            }
        }

        function clearMeta(){
            setMeta({});
        }

    }

})();


