
(function () {

    'use strict';

    angular
        .module('sampleApp')
        .controller('MainConroller', MainConroller)
        .config(moduleConfig);

    /* @ngInject */
    function MainConroller($scope, $state, PageMetaService) {
        $scope.meta = PageMetaService;
        $state.go('home');
    }

    /* @ngInject */
    function moduleConfig($stateProvider, appStates) {

        for (var state in appStates) {
            if(appStates.hasOwnProperty(state)) {
                $stateProvider.state(state, appStates[state]);
            }
        }

    }

})();