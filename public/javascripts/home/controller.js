(function(){

    'use strict';

    angular
        .module('sampleApp')
        .controller('HomeController', HomeController);

    /* @ngInject */
    function HomeController(StepService, PageMetaService, pageDefaults){
        var vm = this;

        //////////

        PageMetaService.setMeta({
            title: pageDefaults.title,
            description: pageDefaults.description,
            ogTitle: pageDefaults.title,
            ogDescription: pageDefaults.description

            //,
            //canonical: 'http://localhost:3000/'
        });

        StepService.getList().then(function(steps){
            vm.steps = steps;
        });

    }

})();

