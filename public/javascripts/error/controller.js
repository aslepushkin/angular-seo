(function(){

    'use strict';

    angular
        .module('sampleApp')
        .controller('ErrorController', ErrorController);

    /* @ngInject */
    function ErrorController(StepService){
        var vm = this;

        StepService.getList().then(function(steps){
           vm.steps = steps;
        });

    }

})();

