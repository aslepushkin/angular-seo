'use strict';

var cache,
    path = require('path'),
    fs = require('fs');

fs.readFile(
    path.join(__dirname, '../data', 'list.json'),
    function(err, data){
        if (err) {
            console.log(err, err.stack);
        } else {
            cache = JSON.parse(data.toString());
            console.log('Cache was loaded');
        }
    }
);

module.exports = function(){ return cache; };