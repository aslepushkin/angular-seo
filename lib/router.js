var express = require('express'),
    router = express.Router(),
    path = require('path'),
    fs = require('fs');



// SPA URLs redirect
router.get('/step/:id', function(req, res, next) {
    fs.readFile(
        path.join(__dirname, '../public', 'index.html'),
        function(err, data){
            if (err) {
                next(err);
            } else {
                res.send(data.toString());
            }
        }
    );

    //res.set('Content-Type', 'text/html');
    //res.status(200);

});


// API URLs handling
router.get('/steps', function(req, res, next) {
    var limit = parseInt(req.query.limit || 100),
        offset = parseInt(req.query.offset || 0);
    res.json(res.locals.cache.slice(offset, offset+limit).map(function(element, index, array){
        return {
            id: element.id,
            title: element.title,
            published: element.published
        }
    }));
});
router.get('/steps/:id', function(req, res, next) {
    var step = res.locals.cache.find(function(element, index, array){
        return element.id == req.params.id;
    });
    if(!step) {
        var err = new Error('Not Found');
        err.status = 404;
        return next(err);
    }
    res.json(step);
});

module.exports = router;

